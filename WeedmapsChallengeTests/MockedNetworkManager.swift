//
//  MockedNetworkManager.swift
//  WeedmapsChallengeTests
//
//  Created by Jonathan Yee on 3/10/20.
//  Copyright © 2020 Weedmaps, LLC. All rights reserved.
//

import CoreLocation
import Foundation
@testable import WeedmapsChallenge


class MockedNetworkManager: NetworkProtocol {
    func getBusinesses(location: CLLocation, completion: @escaping (Result<BusinessWrapper, Error>) -> Void) {
        let businessWrapper = BusinessWrapper(total: 1, businesses: [
            Business(rating: 4, price: "$", phone: "1111111", name: "Four Barrel Coffee", url: "https://www.yelp.com/biz/four-barrel-coffee-san-francisco", imageURL: "http://s3-media2.fl.yelpcdn.com/bphoto/MmgtASP3l_t4tPCL1iAsCg/o.jpg")
        ])
        completion(.success(businessWrapper))
    }


}
