//
//  HomeViewModelTests.swift
//  WeedmapsChallengeTests
//
//  Created by Jonathan Yee on 3/10/20.
//  Copyright © 2020 Weedmaps, LLC. All rights reserved.
//

import CoreLocation
import XCTest
@testable import WeedmapsChallenge


class HomeViewModelTests: XCTestCase {

    private let viewModel = HomeViewModel(network: MockedNetworkManager(), imageDownloader: ImageDownloadManager())

    private let businessWrapper = BusinessWrapper(total: 1, businesses: [
        Business(rating: 4, price: "$", phone: "1111111", name: "Four Barrel Coffee", url: "https://www.yelp.com/biz/four-barrel-coffee-san-francisco", imageURL: "http://s3-media2.fl.yelpcdn.com/bphoto/MmgtASP3l_t4tPCL1iAsCg/o.jpg")
    ])

    override func setUp() {
        super.setUp()
    }

    override func tearDown() {
        super.tearDown()
    }

    func testGetBusinesses() {
        self.viewModel.getBusinesses(location: CLLocation(latitude: 1.0, longitude: 2.0)) { result in
            switch result {
                case .success(let businessWrapper):
                    XCTAssertTrue(businessWrapper == self.businessWrapper)
                case .failure(_):
                    XCTFail()
            }
        }
    }

    func testConfigure() {
        let cell = BusinessCell(frame: .zero)
        let text = """
        Four Barrel Coffee
        Rating: 4.0
        $
        """
        if let business = self.businessWrapper.businesses?.compactMap({ $0 }).first {
            self.viewModel.configure(cell: cell, with: business)
            XCTAssertTrue(cell.textView.text == text)
        } else {
            XCTFail()
        }

    }

}
