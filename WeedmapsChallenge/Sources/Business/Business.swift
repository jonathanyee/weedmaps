//
//  Copyright © 2018 Weedmaps, LLC. All rights reserved.
//

import Foundation


struct Business: Decodable, Equatable {
    var rating: Float?
    var price: String?
    var phone: String?
    var name: String?
    var url: String?
    var imageURL: String?

    enum CodingKeys: String, CodingKey {
        case rating
        case price
        case phone
        case name
        case url
        case imageURL = "image_url"
    }
}
