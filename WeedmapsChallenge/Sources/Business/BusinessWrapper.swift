//
//  BusinessWrapper.swift
//  WeedmapsChallenge
//
//  Created by Jonathan Yee on 3/7/20.
//  Copyright © 2020 Weedmaps, LLC. All rights reserved.
//

import Foundation

struct BusinessWrapper: Decodable, Equatable {
    var total: Int?
    var businesses: [Business?]?

    enum CodingKeys: String, CodingKey {
        case total
        case businesses
    }
}
