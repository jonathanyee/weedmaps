//
//  ImageDownloadOperation.swift
//  WeedmapsChallenge
//
//  Created by Jonathan Yee on 3/8/20.
//  Copyright © 2020 Weedmaps, LLC. All rights reserved.
//

import Foundation
import UIKit

class ImageDownloadOperation: Operation {

    var downloadCallback: ImageDownloadCallback?
    var imageURL: URL
    var indexPath: IndexPath?

    override var isAsynchronous: Bool {
        get {
            return true
        }
    }

    private var _executing = false {
        willSet {
            willChangeValue(for: \.isExecuting)
        }
        didSet {
            didChangeValue(for: \.isExecuting)
        }
    }

    override var isExecuting: Bool {
        return _executing
    }

    private var _finished = false {
        willSet {
            willChangeValue(for: \.isFinished)
        }
        didSet {
            didChangeValue(for: \.isFinished)
        }
    }

    override var isFinished: Bool {
        return self._finished
    }

    func executing(_ executing: Bool) {
        self._executing = executing
    }

    func finish(_ finished: Bool) {
        self._finished = finished
    }

    init(imageURL: URL, indexPath: IndexPath?) {
        self.imageURL = imageURL
        self.indexPath = indexPath
    }

    override func main() {
        guard self.isCancelled == false else {
            self.finish(true)
            return
        }

        self.executing(true)
        self.downloadImage()
    }

    func downloadImage() {
        let session = URLSession.shared
        let task = session.downloadTask(with: self.imageURL) { [weak self] (url, response, error) in
            if let locationURL = url,
                let data = try? Data(contentsOf: locationURL) {
                let image = UIImage(data: data)
                self?.downloadCallback?(image, self?.imageURL, self?.indexPath, error)
            }
            self?.finish(true)
            self?.executing(false)
        }
        task.resume()
    }
}
