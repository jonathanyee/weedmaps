//
//  ImageDownloadManager.swift
//  WeedmapsChallenge
//
//  Created by Jonathan Yee on 3/8/20.
//  Copyright © 2020 Weedmaps, LLC. All rights reserved.
//

import Foundation
import UIKit

typealias ImageDownloadCallback = (_ image: UIImage?, _ url: URL?, _ indexPath: IndexPath?, _ error: Error?) -> Void

class ImageDownloadManager {
    private lazy var queue: OperationQueue = {
        let queue = OperationQueue()
        queue.name = "com.weedmaps.imagequeue"
        queue.qualityOfService = .userInitiated
        return queue
    }()

    private let cache = NSCache<NSString, UIImage>()

    func download(url: URL, indexPath: IndexPath?, completion: @escaping ImageDownloadCallback) {
        if let cachedImage = self.cache.object(forKey: url.absoluteString as NSString) {
            completion(cachedImage, url, indexPath, nil)
        } else {
            if let op = self.getOperation(by: url) {
                op.queuePriority = .high
            } else {
                let operation = ImageDownloadOperation(imageURL: url, indexPath: indexPath)
                if indexPath == nil {
                    operation.queuePriority = .veryHigh
                }
                operation.downloadCallback = { [weak self] (image, url, indexPath, error) in
                    if let image = image {
                        self?.cache.setObject(image, forKey: NSString(string: url?.absoluteString ?? ""))
                    }
                    completion(image, url, indexPath, error)
                }
                self.queue.addOperation(operation)
            }
        }
    }

    func lowerPriority(for url: URL) {
        if let operation = self.getOperation(by: url) {
            operation.queuePriority = .low
        }
    }

    private func getOperation(by url: URL) -> ImageDownloadOperation? {
        let operations = self.queue.operations.compactMap { $0 as? ImageDownloadOperation }
        return operations.first {
            $0.imageURL == url &&
            $0.isFinished == false &&
            $0.isExecuting == true
        }
    }
}
