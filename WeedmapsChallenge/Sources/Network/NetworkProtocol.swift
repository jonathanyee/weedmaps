//
//  NetworkProtocol.swift
//  WeedmapsChallenge
//
//  Created by Jonathan Yee on 3/7/20.
//  Copyright © 2020 Weedmaps, LLC. All rights reserved.
//

import CoreLocation
import Foundation

protocol NetworkProtocol {
    func getBusinesses(location: CLLocation, completion: @escaping (Result<BusinessWrapper, Error>) -> Void)
}
