//
//  NetworkManager.swift
//  WeedmapsChallenge
//
//  Created by Jonathan Yee on 3/7/20.
//  Copyright © 2020 Weedmaps, LLC. All rights reserved.
//

import  CoreLocation
import Foundation

class NetworkManager: NetworkProtocol {

    enum GenericError: Error {
        case error
    }

    private let decoder = JSONDecoder()

    func getBusinesses(location: CLLocation, completion: @escaping (Result<BusinessWrapper, Error>) -> Void) {
        let params = [
            URLQueryItem(name: "latitude", value: String(location.coordinate.latitude)),
            URLQueryItem(name: "longitude", value: String(location.coordinate.longitude))
        ]
        self.get("businesses/search", params: params, completion: completion)
    }

    private func get<T: Decodable>(_ path: String,
                     params: [URLQueryItem]? = nil,
                     completion: @escaping (Result<T, Error>) -> Void) {
        var urlComponents = URLComponents()
        urlComponents.scheme = "https"
        urlComponents.host = "api.yelp.com"
        urlComponents.path = "/v3/\(path)"
        urlComponents.queryItems = params

        guard let url = urlComponents.url else {
            return
        }

        print("\(url)")

        var request = URLRequest(url: url)
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("Bearer ajCeEhY-gfN2p8jsFbdWPhEpBAwQkSOXtpW0emVtp1ntcTZKc-c5kfvKT1Rik57f6ds_eC_CG6dBWSWNti-9KGIybYOUxX09Nj5pWFljgLWrTuy5EclKKZm5S_djXnYx", forHTTPHeaderField: "Authorization")

        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in

            if let json = data,
                let decodedJson = try? self.decoder.decode(T.self, from: json),
                error == nil {
                completion(.success(decodedJson))
            }
            else {
                completion(.failure(GenericError.error))
            }
        }
        task.resume()
    }
}
