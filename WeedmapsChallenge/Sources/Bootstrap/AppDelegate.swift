//
//  Copyright © 2018 Weedmaps, LLC. All rights reserved.
//

import UIKit


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    // MARK: Properties
    
    var window: UIWindow?

    // MARK: Lifecycle
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        let frame = UIScreen.main.bounds
        window = UIWindow(frame: frame)

        let network = NetworkManager()
        let location = LocationManager()
        let imageDownloader = ImageDownloadManager()

        let homeViewModel = HomeViewModel(network: network, imageDownloader: imageDownloader)
        window?.rootViewController = HomeViewController(viewModel: homeViewModel, location: location)
        window?.makeKeyAndVisible()
        return true
    }
}

