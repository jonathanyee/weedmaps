//
//  LocationManager.swift
//  WeedmapsChallenge
//
//  Created by Jonathan Yee on 3/8/20.
//  Copyright © 2020 Weedmaps, LLC. All rights reserved.
//

import Foundation
import CoreLocation

class LocationManager: NSObject {
    private let locationManager = CLLocationManager()

    var getCurrentLocationCallback: ((CLLocation) -> Void)?
    var hasPermissionCallback: ((Bool) -> Void)?

    override init() {
        super.init()
        self.locationManager.delegate = self
    }

    func hasPermission() -> Bool? {
        let status = CLLocationManager.authorizationStatus()
        if status == .authorizedAlways || status == .authorizedWhenInUse {
            return true
        } else if status == .notDetermined {
            self.locationManager.requestWhenInUseAuthorization()
            return nil
        } else {
            return false
        }
    }

    func getCurrentLocation() {
        self.locationManager.requestLocation()
    }
}

extension LocationManager: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
            case .authorizedAlways, .authorizedWhenInUse:
                self.hasPermissionCallback?(true)
            default:
                self.hasPermissionCallback?(false)
        }
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print("\(locations)")
        if let location = locations.first {
            self.getCurrentLocationCallback?(location)
        }
    }

    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("\(error)")
    }
}

