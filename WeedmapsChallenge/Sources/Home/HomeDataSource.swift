//
//  HomeDataSource.swift
//  WeedmapsChallenge
//
//  Created by Jonathan Yee on 3/9/20.
//  Copyright © 2020 Weedmaps, LLC. All rights reserved.
//

import Foundation
import UIKit

class HomeDataSource: NSObject, UICollectionViewDataSource {

    var businesses: [Business]?

    private let viewModel: HomeViewModel

    init(viewModel: HomeViewModel) {
        self.viewModel = viewModel
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.businesses?.count ?? 0
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard
            let businessCell = collectionView.dequeueReusableCell(withReuseIdentifier: BusinessCell.reuseIdentifier, for: indexPath) as? BusinessCell,
            let business = self.businesses?[indexPath.row]
        else {
            return UICollectionViewCell()
        }

        self.viewModel.configure(cell: businessCell, with: business)

        return businessCell
    }

}
