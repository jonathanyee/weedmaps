//
//  HomeViewModel.swift
//  WeedmapsChallenge
//
//  Created by Jonathan Yee on 3/9/20.
//  Copyright © 2020 Weedmaps, LLC. All rights reserved.
//

import CoreLocation
import Foundation

class HomeViewModel {

    private let network: NetworkProtocol
    private let imageDownloader: ImageDownloadManager

    init(network: NetworkProtocol, imageDownloader: ImageDownloadManager) {
        self.network = network
        self.imageDownloader = imageDownloader
    }

    func getBusinesses(location: CLLocation, completion: @escaping (Result<BusinessWrapper, Error>) -> Void) {
        self.network.getBusinesses(location: location) { result in
            completion(result)
        }
    }

    func willDisplay(cell: BusinessCell, url: URL, at indexPath: IndexPath) {
        self.imageDownloader.download(url: url, indexPath: indexPath) { (image, url, indexPath, error) in
            DispatchQueue.main.async {
                cell.imageView.image = image
            }
        }
    }

    func configure(cell: BusinessCell, with business: Business) {
        cell.textView.text = """
        \(business.name ?? "")
        Rating: \(business.rating ?? 0)
        \(business.price ?? "")
        """
    }

    func didEndDisplaying(url: URL) {
        self.imageDownloader.lowerPriority(for: url)
    }
}
