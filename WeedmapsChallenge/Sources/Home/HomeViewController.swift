//
//  Copyright © 2018 Weedmaps, LLC. All rights reserved.
//

import UIKit
import SafariServices

class HomeViewController: UIViewController {

    // MARK: Properties
    
    private var collectionView: UICollectionView?
    
    private var searchController = UISearchController(searchResultsController: nil)
    private var searchResults = [Business]()
    private var searchDataTask: URLSessionDataTask?

    private let viewModel: HomeViewModel
    private let locationManager: LocationManager

    private var homeDataSource: HomeDataSource

    init(viewModel: HomeViewModel, location: LocationManager) {
        self.viewModel = viewModel
        self.locationManager = location
        self.homeDataSource = HomeDataSource(viewModel: self.viewModel)

        super.init(nibName: nil, bundle: nil)

        self.locationManager.hasPermissionCallback = { [weak self] hasPermission in
            if hasPermission {
                self?.locationManager.getCurrentLocation()
            }
        }

        self.locationManager.getCurrentLocationCallback = { [weak self] location in
            self?.viewModel.getBusinesses(location: location) { [weak self] result in
                switch result {
                    case .success(let businessWrapper):
                        self?.homeDataSource.businesses = businessWrapper.businesses?.compactMap { $0 }
                        DispatchQueue.main.async {
                            self?.collectionView?.reloadData()
                        }
                    case .failure(let error):
                        print("\(error)")
                }
            }
        }
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = UIColor.white

        self.setupCollectionView()
        self.layoutCollectionView()

        let hasPermission = self.locationManager.hasPermission()
        if hasPermission == true {
            self.locationManager.getCurrentLocation()
        } else if hasPermission == false {
            self.showLocationDeniedAlert()
        }
    }

    private func setupCollectionView() {
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .vertical
        flowLayout.itemSize = CGSize(width: 200, height: 200)

        self.collectionView = UICollectionView(frame: self.view.bounds, collectionViewLayout: flowLayout)
        self.collectionView?.dataSource = self.homeDataSource
        self.collectionView?.delegate = self
        self.collectionView?.register(BusinessCell.self, forCellWithReuseIdentifier: BusinessCell.reuseIdentifier)
        self.collectionView?.backgroundColor = UIColor.white
    }

    private func layoutCollectionView() {
        guard
            let collectionView = self.collectionView
        else {
            return
        }
        self.view.addSubview(collectionView)

        collectionView.translatesAutoresizingMaskIntoConstraints = false

        NSLayoutConstraint.activate([
            collectionView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
            collectionView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor),
            collectionView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor),
            collectionView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor),
        ])
    }

    private func showLocationDeniedAlert() {
        let alert = UIAlertController(title: "Go to Settings to enable Location",
                                      message: "We need access to your location to show you drug prices in your area.",
                                      preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Settings", style: .default) { action in
            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                return
            }

            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, completionHandler: nil)
            }
        })
        alert.addAction(UIAlertAction(title: "Close", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}

// MARK: UISearchResultsUpdating

extension HomeViewController: UISearchResultsUpdating {

    func updateSearchResults(for searchController: UISearchController) {
        // IMPLEMENT: Be sure to consider things like network errors
        // and possible rate limiting from the Yelp API. If the user types
        // very quickly, how will you prevent unnecessary requests from firing
        // off? Be sure to leverage the searchDataTask and use it wisely!
    }
}

// MARK: UICollectionViewDelegate

extension HomeViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // IMPLEMENT:
        // 1a) Present the user with a UIAlertController (action sheet style) with options
        // to either display the Business's Yelp page in a WKWebView OR bump the user out to
        // Safari. Both options should display the Business's Yelp page details

        guard
            let businessURL = self.homeDataSource.businesses?[indexPath.row].url,
            let url = URL(string: businessURL)
        else {
            return
        }

        let alertViewController = UIAlertController(title: "Choose how to view", message: nil, preferredStyle: .actionSheet)
        let webViewAction = UIAlertAction(title: "WebView", style: .default) { [weak self] action in
            let viewController = SFSafariViewController(url: url)
            self?.present(viewController, animated: true, completion: nil)
        }
        let safariAction = UIAlertAction(title: "Safari", style: .default) { _ in
             UIApplication.shared.open(url)
        }
        alertViewController.addAction(webViewAction)
        alertViewController.addAction(safariAction)
        self.present(alertViewController, animated: true, completion: nil)
    }

    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard
            let urlString = self.homeDataSource.businesses?[indexPath.row].imageURL,
            let url = URL(string: urlString),
            let businessCell = cell as? BusinessCell
        else {
            return
        }

        self.viewModel.willDisplay(cell: businessCell, url: url, at: indexPath)
    }

    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard
            let urlString = self.homeDataSource.businesses?[indexPath.row].url,
            let url = URL(string: urlString)
        else {
            return
        }

        self.viewModel.didEndDisplaying(url: url)
    }
}
